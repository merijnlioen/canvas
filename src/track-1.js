import TrackBase from './track-base';
import Vector from './vector';

export default class TrackOne extends TrackBase {
    constructor(){
        super()
        this.raceTrackImage.src = './img/track_1.svg'

        this.trackParts = [
        {
            start: new Vector(391, 57),
            curve: new Vector(303, 84),
            end: new Vector(266, 115),
        },
        
        {
            start: new Vector(266, 115),
            curve: new Vector(255, 126),
            end: new Vector(241, 122),
        },
        
        {
            start: new Vector(241, 122),
            curve: new Vector(221, 123),
            end: new Vector(212, 140),
        },
        
        {
            start: new Vector(212, 140),
            curve: new Vector(210, 155),
            end: new Vector(200, 159),
        },
        
        {
            start: new Vector(200, 159),
            curve: new Vector(172, 179),
            end: new Vector(155, 199),
        },
        
        {
            start: new Vector(155, 199),
            curve: new Vector(122, 231),
            end: new Vector(108, 251),
        },
        
        {
            start: new Vector(108, 251),
            curve: new Vector(89, 289),
            end: new Vector(129, 279),
        },
        
        {
            start: new Vector(129, 279),
            curve: new Vector(181, 253),
            end: new Vector(220, 229),
        },
        
        {
            start: new Vector(220, 229),
            curve: new Vector(234, 218),
            end: new Vector(243, 233),
        },
        
        {
            start: new Vector(243, 233),
            curve: new Vector(257, 245),
            end: new Vector(283, 230),
        },
        
        {
            start: new Vector(283, 230),
            curve: new Vector(325, 200),
            end: new Vector(370, 204),
        },
        
        {
            start: new Vector(370, 204),
            curve: new Vector(449, 225),
            end: new Vector(467, 270),
        },
        
        {
            start: new Vector(467, 270),
            curve: new Vector(501, 334),
            end: new Vector(552, 352),
        },
        
        {
            start: new Vector(552, 352),
            curve: new Vector(577, 367),
            end: new Vector(599, 337),
        },
        
        {
            start: new Vector(599, 337),
            curve: new Vector(626, 308),
            end: new Vector(586, 290),
        },
        
        {
            start: new Vector(586, 290),
            curve: new Vector(550, 275),
            end: new Vector(564, 239),
        },
        
        {
            start: new Vector(564, 239),
            curve: new Vector(578, 208),
            end: new Vector(536, 201),
        },
        
        {
            start: new Vector(536, 201),
            curve: new Vector(461, 173),
            end: new Vector(461, 141),
        },
        
        {
            start: new Vector(461, 141),
            curve: new Vector(457, 114),
            end: new Vector(514, 103),
        },
        
        {
            start: new Vector(514, 103),
            curve: new Vector(593, 76),
            end: new Vector(612, 118),
        },
        
        {
            start: new Vector(612, 118),
            curve: new Vector(628, 134),
            end: new Vector(644, 130),
        },
        
        {
            start: new Vector(644, 130),
            curve: new Vector(668, 111),
            end: new Vector(643, 77),
        },
        
        {
            start: new Vector(643, 77),
            curve: new Vector(599, 17),
            end: new Vector(576, 30),
        },
        
        {
            start: new Vector(576, 30),
            curve: new Vector(558, 37),
            end: new Vector(543, 19),
        },
        
        {
            start: new Vector(543, 19),
            curve: new Vector(536, 4),
            end: new Vector(510, 20),
        },
        
        {
            start: new Vector(510, 20),
            curve: new Vector(452, 42),
            end: new Vector(391, 57),
        },
        
        ]

        this.stops = [
            this.trackParts[0].start,
            this.trackParts[5].start,
            this.trackParts[11].start,
            this.trackParts[19].start,
        ]
    }
}

