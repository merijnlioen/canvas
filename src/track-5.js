import TrackBase from './track-base';
import Vector from './vector';

export default class TrackFive extends TrackBase {
    constructor(){
        super()
        this.raceTrackImage.src = './img/track_5.svg'

        this.trackParts = [
            {
                start: new Vector(379, 80),
                curve: new Vector(286, 108),
                end: new Vector(221, 129),
            },
            
            {
                start: new Vector(221, 129),
                curve: new Vector(151, 153),
                end: new Vector(91, 168),
            },
            
            {
                start: new Vector(91, 168),
                curve: new Vector(44, 178),
                end: new Vector(45, 219),
            },
            
            {
                start: new Vector(45, 219),
                curve: new Vector(53, 252),
                end: new Vector(78, 239),
            },
            
            {
                start: new Vector(78, 239),
                curve: new Vector(107, 232),
                end: new Vector(111, 218),
            },
            
            {
                start: new Vector(111, 218),
                curve: new Vector(120, 206),
                end: new Vector(129, 219),
            },
            
            {
                start: new Vector(129, 219),
                curve: new Vector(134, 248),
                end: new Vector(149, 259),
            },
            
            {
                start: new Vector(149, 259),
                curve: new Vector(167, 278),
                end: new Vector(221, 256),
            },
            
            {
                start: new Vector(221, 256),
                curve: new Vector(292, 232),
                end: new Vector(373, 228),
            },
            
            {
                start: new Vector(373, 228),
                curve: new Vector(401, 229),
                end: new Vector(415, 251),
            },
            
            {
                start: new Vector(415, 251),
                curve: new Vector(436, 271),
                end: new Vector(471, 255),
            },
            
            {
                start: new Vector(471, 255),
                curve: new Vector(502, 235),
                end: new Vector(526, 262),
            },
            
            {
                start: new Vector(526, 262),
                curve: new Vector(538, 276),
                end: new Vector(558, 276),
            },
            
            {
                start: new Vector(558, 276),
                curve: new Vector(586, 277),
                end: new Vector(592, 316),
            },
            
            {
                start: new Vector(592, 316),
                curve: new Vector(603, 365),
                end: new Vector(618, 390),
            },
            
            {
                start: new Vector(618, 390),
                curve: new Vector(631, 425),
                end: new Vector(652, 390),
            },
            
            {
                start: new Vector(652, 390),
                curve: new Vector(672, 358),
                end: new Vector(644, 359),
            },
            
            {
                start: new Vector(644, 359),
                curve: new Vector(630, 356),
                end: new Vector(635, 309),
            },
            
            {
                start: new Vector(635, 309),
                curve: new Vector(646, 229),
                end: new Vector(658, 154),
            },
            
            {
                start: new Vector(658, 154),
                curve: new Vector(671, 82),
                end: new Vector(648, 50),
            },
            
            {
                start: new Vector(648, 50),
                curve: new Vector(634, 49),
                end: new Vector(628, 23),
            },
            
            {
                start: new Vector(628, 23),
                curve: new Vector(620, 8),
                end: new Vector(589, 19),
            },
            
            {
                start: new Vector(589, 19),
                curve: new Vector(481, 54),
                end: new Vector(379, 80),
            },
            
        ]

        this.stops = [
            this.trackParts[0].start,
            this.trackParts[7].start,
            this.trackParts[9].start,
            this.trackParts[19].start,
        ]
    }
}

