export default class Vector {
    constructor(x, y) {
        this.x = x
        this.y = y
    }

    multiply(factor) {
        this.x = this.x * factor
        this.y = this.y * factor
    }

    equals(vector) {
        return this.x === vector.x && this.y === vector.y
    }
}