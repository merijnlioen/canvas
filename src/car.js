import Vector from "./vector";

export default class Car {
    constructor() {
        this.position = new Vector(0,0)
        this.carImage = new Image()
        this.carImage.src = './img/car.svg'
        this.raceCarSizeFactor = 7
    }

    draw(canvas, ctx, currentPoint, nextPoint) {
        let rotation = this.calculateRotation(currentPoint, nextPoint)
        let size = this.calculateSize(canvas)
        let position = this.calculateCenterPosition(currentPoint, size)

        ctx.save()
        ctx.translate(currentPoint.x, currentPoint.y)
        ctx.rotate(rotation)
        ctx.translate(-currentPoint.x, -currentPoint.y)
        ctx.drawImage(this.carImage, position.x, position.y, size.width, size.height)
        ctx.restore()
    }

    calculateRotation(currentPoint, nextPoint) {
        let deltaX = nextPoint.x - currentPoint.x
        let deltaY = nextPoint.y - currentPoint.y

        let rotationRad = Math.atan2(deltaY, deltaX)

        return rotationRad
    }

    calculateSize(canvas) {
        let ratioCarImage = this.carImage.height / this.carImage.width

        let width = canvas.width / this.carImage.width * this.raceCarSizeFactor
        let height = width * ratioCarImage

        return {
            width,
            height
        }
    }

    calculateCenterPosition(point, size){
        let x = point.x - (size.width / 2)
        let y = point.y - (size.height / 2)

        return new Vector(x, y)
    }
}