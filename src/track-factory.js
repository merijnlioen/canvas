import TrackOne from "./track-1";
import TrackTwo from "./track-2";
import TrackThree from "./track-3";
import TrackFour from "./track-4";
import TrackFive from "./track-5";
import TrackSix from "./track-6";

export default class TrackFactory {
    initRaceTrack(trackId) {
        switch(trackId) {
            case 1:
                return new TrackOne()
            case 2:
                return new TrackTwo()
            case 3:
                return new TrackThree()
            case 4:
                return new TrackFour()
            case 5:
                return new TrackFive()
            case 6:
                return new TrackSix()
            default:
                break;
        }
    }
}