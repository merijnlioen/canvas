import TrackBase from './track-base';
import Vector from './vector';

export default class TrackSix extends TrackBase {
    constructor(){
        super()
        this.raceTrackImage.src = './img/track_6.svg'

        this.trackParts = [
            {
                start: new Vector(487, 97),
                curve: new Vector(362, 165),
                end: new Vector(362, 206),
            },
            
            {
                start: new Vector(362, 206),
                curve: new Vector(379, 360),
                end: new Vector(362, 376),
            },
            
            {
                start: new Vector(362, 376),
                curve: new Vector(361, 385),
                end: new Vector(342, 391),
            },
            
            {
                start: new Vector(342, 391),
                curve: new Vector(333, 389),
                end: new Vector(331, 410),
            },
            
            {
                start: new Vector(331, 410),
                curve: new Vector(330, 430),
                end: new Vector(293, 435),
            },
            
            {
                start: new Vector(293, 435),
                curve: new Vector(175, 439),
                end: new Vector(65, 437),
            },
            
            {
                start: new Vector(65, 437),
                curve: new Vector(39, 437),
                end: new Vector(19, 411),
            },
            
            {
                start: new Vector(19, 411),
                curve: new Vector(5, 360),
                end: new Vector(45, 363),
            },
            
            {
                start: new Vector(45, 363),
                curve: new Vector(85, 361),
                end: new Vector(108, 364),
            },
            
            {
                start: new Vector(108, 364),
                curve: new Vector(127, 367),
                end: new Vector(139, 352),
            },
            
            {
                start: new Vector(139, 352),
                curve: new Vector(151, 340),
                end: new Vector(171, 355),
            },
            
            {
                start: new Vector(171, 355),
                curve: new Vector(192, 373),
                end: new Vector(214, 343),
            },
            
            {
                start: new Vector(214, 343),
                curve: new Vector(237, 314),
                end: new Vector(269, 343),
            },
            
            {
                start: new Vector(269, 343),
                curve: new Vector(297, 361),
                end: new Vector(319, 330),
            },
            
            {
                start: new Vector(319, 330),
                curve: new Vector(338, 304),
                end: new Vector(326, 241),
            },
            
            {
                start: new Vector(326, 241),
                curve: new Vector(296, 169),
                end: new Vector(330, 141),
            },
            
            {
                start: new Vector(330, 141),
                curve: new Vector(352, 111),
                end: new Vector(374, 139),
            },
            
            {
                start: new Vector(374, 139),
                curve: new Vector(391, 161),
                end: new Vector(426, 177),
            },
            
            {
                start: new Vector(426, 177),
                curve: new Vector(471, 198),
                end: new Vector(493, 249),
            },
            
            {
                start: new Vector(493, 249),
                curve: new Vector(503, 281),
                end: new Vector(525, 285),
            },
            
            {
                start: new Vector(525, 285),
                curve: new Vector(559, 288),
                end: new Vector(531, 222),
            },
            
            {
                start: new Vector(531, 222),
                curve: new Vector(508, 128),
                end: new Vector(543, 114),
            },
            
            {
                start: new Vector(543, 114),
                curve: new Vector(598, 111),
                end: new Vector(633, 118),
            },
            
            {
                start: new Vector(633, 118),
                curve: new Vector(682, 113),
                end: new Vector(683, 70),
            },
            
            {
                start: new Vector(683, 70),
                curve: new Vector(685, 3),
                end: new Vector(612, 26),
            },
            
            {
                start: new Vector(612, 26),
                curve: new Vector(559, 49),
                end: new Vector(487, 97),
            },
            
        ]

        this.stops = [
            this.trackParts[0].start,
            this.trackParts[12].start,
            this.trackParts[20].start,
            this.trackParts[22].start,
        ]
    }
}