import Race from './race.js';
import RecordTrack from './record-track';

export default class App {
    constructor() {
        let track = new Race()

        track.init(6).then(() => {
            track.setCurrentTrackPosition(1).then(() => {
                setTimeout(() => {
                    track.setCurrentTrackPosition(2).then(() => {
                        setTimeout(() => {
                            track.setCurrentTrackPosition(3).then(() => {
                                setTimeout(() => {
                                    track.setCurrentTrackPosition(0)
                                }, 1000)
                            })
                        }, 1000)
                    })
                }, 1000)
            })
        })
    }
}

new App()