import TrackBase from './track-base';
import Vector from './vector';

export default class TrackTwo extends TrackBase {
    constructor(){
        super()
        this.raceTrackImage.src = './img/track_2.svg'

        this.trackParts = [
            {
                start: new Vector(400, 253),
                curve: new Vector(481, 255),
                end: new Vector(486, 286),
            },
            
            {
                start: new Vector(486, 286),
                curve: new Vector(489, 324),
                end: new Vector(525, 322),
            },
            
            {
                start: new Vector(525, 322),
                curve: new Vector(594, 322),
                end: new Vector(651, 300),
            },
            
            {
                start: new Vector(651, 300),
                curve: new Vector(672, 292),
                end: new Vector(651, 263),
            },
            
            {
                start: new Vector(651, 263),
                curve: new Vector(637, 245),
                end: new Vector(669, 220),
            },
            
            {
                start: new Vector(669, 220),
                curve: new Vector(699, 197),
                end: new Vector(685, 139),
            },
            
            {
                start: new Vector(685, 139),
                curve: new Vector(676, 102),
                end: new Vector(659, 77),
            },
            
            {
                start: new Vector(659, 77),
                curve: new Vector(652, 65),
                end: new Vector(656, 52),
            },
            
            {
                start: new Vector(656, 52),
                curve: new Vector(656, 40),
                end: new Vector(642, 40),
            },
            
            {
                start: new Vector(642, 40),
                curve: new Vector(616, 37),
                end: new Vector(599, 19),
            },
            
            {
                start: new Vector(599, 19),
                curve: new Vector(580, 4),
                end: new Vector(531, 29),
            },
            
            {
                start: new Vector(531, 29),
                curve: new Vector(481, 49),
                end: new Vector(468, 104),
            },
            
            {
                start: new Vector(468, 104),
                curve: new Vector(456, 153),
                end: new Vector(399, 170),
            },
            
            {
                start: new Vector(399, 170),
                curve: new Vector(333, 169),
                end: new Vector(321, 146),
            },
            
            {
                start: new Vector(321, 146),
                curve: new Vector(310, 118),
                end: new Vector(271, 122),
            },
            
            {
                start: new Vector(271, 122),
                curve: new Vector(208, 125),
                end: new Vector(155, 182),
            },
            
            {
                start: new Vector(155, 182),
                curve: new Vector(110, 220),
                end: new Vector(121, 247),
            },
            
            {
                start: new Vector(121, 247),
                curve: new Vector(140, 327),
                end: new Vector(170, 321),
            },
            
            {
                start: new Vector(170, 321),
                curve: new Vector(223, 308),
                end: new Vector(223, 271),
            },
            
            {
                start: new Vector(223, 271),
                curve: new Vector(220, 208),
                end: new Vector(294, 236),
            },
            
            {
                start: new Vector(294, 236),
                curve: new Vector(346, 250),
                end: new Vector(400, 253),
            },
            
        ]

        this.stops = [
            this.trackParts[0].start,
            this.trackParts[6].start,
            this.trackParts[13].start,
            this.trackParts[16].start,
        ]
    }
}

