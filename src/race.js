import Car from "./car";
import TrackFactory from "./track-factory";

export default class Race {
    init(trackNumber) {
        return new Promise(resolve => {
            this.canvas = document.getElementById("bose-game")
            this.canvas.width = this.canvas.clientWidth
            this.canvas.height = this.canvas.clientHeight
            this.ctx = this.canvas.getContext("2d")

            this.trackNumber = trackNumber
            this.raceCar = new Car()
            this.trackFactory = new TrackFactory()
            this.raceTrack = this.trackFactory.initRaceTrack(this.trackNumber)

            this.activeTrackPart = this.raceTrack.getNextTrackPart()
            this.positionOnTrackPart = 0
            this.stopAtPosition = 0
            this.speed = 8
            this.playing = false
            this.isDevMode = false;
    
            this.bindEvents()

            this.raceTrack.calculateRelativeCoordinates(this.canvas).then(() => {
                resolve()
            })
        })
    }

    bindEvents() {
        window.addEventListener('resize', () => this.recalculateCanvasAndTrackSize())
    }

    recalculateCanvasAndTrackSize() {
        this.canvas.width = this.canvas.clientWidth
        this.canvas.height = this.canvas.clientHeight

        let currentTrackPartIndex = this.raceTrack.currentTrackPartIndex

        this.raceTrack = this.trackFactory.initRaceTrack(this.trackNumber)
        this.raceTrack.currentTrackPartIndex = currentTrackPartIndex
        this.raceTrack.calculateRelativePosition(this.canvas)
    }

    start(callback) {
        window.requestAnimationFrame(() => this.draw(callback))
    }

    draw(callback) {
        if(!this.playing) return 

        window.requestAnimationFrame(() => this.draw(callback))
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)

        if(this.positionOnTrackPart < 100) {
            this.positionOnTrackPart += this.speed
        } else {
            this.positionOnTrackPart = 0

            this.activeTrackPart = this.raceTrack.getNextTrackPart()

            let startVector =this.activeTrackPart.start
            let stopVector = this.raceTrack.stops[this.stopAtPosition]

            this.playing = !startVector.equals(stopVector)

            if(!this.playing && callback != null) {
                callback()
            }
        }

        let currentPoint = this.calculatePointForTrackPostion(this.positionOnTrackPart)
        let nextPoint = this.calculatePointForTrackPostion(this.positionOnTrackPart + 5)

        this.raceTrack.draw(this.canvas, this.ctx)
        this.raceCar.draw(this.canvas, this.ctx, currentPoint, nextPoint)

        if(this.isDevMode) {
            this.raceTrack.drawPath(this.ctx)
        }
    }

    calculatePointForTrackPostion(position = 0) {
        return this.raceTrack.calculateQuadraticBezierAtPosition(
            this.activeTrackPart.start,
            this.activeTrackPart.curve,
            this.activeTrackPart.end,
            position
        )
    }

    setCurrentTrackPosition(trackPosition) {
        this.stopAtPosition = trackPosition
        this.playing = true

        return new Promise(resolve => {
            this.start(resolve)
        })
    }
}