export default class RecordTrack {
    constructor() {
        this.recording = false

        this.recordButton = document.getElementById('record__button')
        this.resetButton = document.getElementById('record__reset-button')
        this.trackDropdown = document.getElementById('select-track')
        this.canvas = document.getElementById('bose-game')
        this.ctx = this.canvas.getContext('2d')
        this.amountOfPathPoints = 0
        this.trackPathLine = []

        this.raceTracks = [{
            name: 'Track 1',
            path: './img/track_1.svg'
        },{
            name: 'Track 2',
            path: './img/track_2.svg'
        },{
            name: 'Track 3',
            path: './img/track_3.svg'
        },{
            name: 'Track 4',
            path: './img/track_4.svg'
        },{
            name: 'Track 5',
            path: './img/track_5.svg'
        },{
            name: 'Track 6',
            path: './img/track_6.svg'
        },]
    }

    init() {
        this.recalculateCanvasAndTrackSize()
        this.loadTracks()
        this.bindEvents()
    }

    bindEvents() {
        window.addEventListener('resize', () => this.test())
        this.canvas.addEventListener('click', e => this.createTrackPath(e))
        this.recordButton.addEventListener('click', () => this.toggleTrackRecording())
        this.resetButton.addEventListener('click', () => this.resetPath())
        this.trackDropdown.addEventListener('change', () => this.updateTrack())
    }

    recalculateCanvasAndTrackSize() {
        this.canvas.width = this.canvas.clientWidth
        this.canvas.height = this.canvas.clientHeight
        this.updateTrack()
    }

    test() {
        this.canvas.width = this.trackImage.width
        this.canvas.height = this.trackImage.height
        this.updateTrack()
    }

    loadTracks() {
        this.raceTracks.map((raceTrack) => {
            this.trackDropdown.insertAdjacentHTML('beforeend', `<option value='${raceTrack.path}'>${raceTrack.name}</option>`)
        })
    }

    updateTrack() {
        this.getTrackImage().then((image) => {
            let size = this.calculateImageSize(image)
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
            this.ctx.drawImage(image, 0, 0, size.width, size.height)
        })
    }

    getTrackImage() {
        this.trackImage = new Image()
        this.trackImage.src = this.trackDropdown.value

        return new Promise(resolve => {
            this.trackImage.addEventListener('load', () => {
                resolve(this.trackImage)
            })
        })
    }

    calculateImageSize(img) {
        let ratio = img.height / img.width
        let width = this.canvas.width
        let height = width * ratio

        if (height > this.canvas.height) {
            height = this.canvas.height
            width = height / ratio
        }

        return {
            width,
            height
        }
    }

    toggleTrackRecording() {
        if(this.trackDropdown.value === '') return window.alert('Select a track first')

        this.recordButton.classList.toggle('record__button--stop')
        this.recording = !this.recording

        if (this.recording) { 
            this.trackRecordingOutput = window.open('about:blank', 'Test', '_blank')
        } else {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        }
    }

    createTrackPath(e) {
        if(!this.recording || !this.trackRecordingOutput) return alert('Please note that you are not recording')
        var clickedPosition = this.getClickedPosition(e)

        switch(this.amountOfPathPoints % 3) {
            case 0:
                this.trackPathLine.start = { x:clickedPosition.x, y:clickedPosition.y}
                break
            case 1:
                this.trackPathLine.end = { x:clickedPosition.x, y:clickedPosition.y}
                break
            case 2: 
                this.trackPathLine.curve = { x:clickedPosition.x, y:clickedPosition.y}
                this.updatePath()
                break
            default:
                break
        }

        this.amountOfPathPoints++
    }

    updatePath() {
        this.trackRecordingOutput.document.write(
            `<pre>
            {
                start: new Vector(${this.trackPathLine.start.x}, ${this.trackPathLine.start.y}),
                curve: new Vector(${this.trackPathLine.curve.x}, ${this.trackPathLine.curve.y}),
                end: new Vector(${this.trackPathLine.end.x}, ${this.trackPathLine.end.y}),
            },
            </pre>`
        )

        this.drawPath(this.trackPathLine)
                
        // Make sure that the start position is the end position of the previous line
        this.trackPathLine.start = this.trackPathLine.end
        this.amountOfPathPoints++
    }

    drawPath(path) {
        this.ctx.strokeStyle = 'red'
        this.ctx.lineWidth = 10
        this.ctx.beginPath()
        this.ctx.moveTo(path.start.x,path.start.y)
        this.ctx.quadraticCurveTo(path.curve.x, path.curve.y, path.end.x, path.end.y)
        this.ctx.stroke()
    }

    getClickedPosition(e) {
        var rect = this.canvas.getBoundingClientRect()

        return {
          x: e.clientX - rect.left,
          y: e.clientY - rect.top
        };      
    }

    resetPath() {
        this.updateTrack()
        this.amountOfPathPoints = 0
        this.trackPathLine = []
        this.toggleTrackRecording()
    }
}