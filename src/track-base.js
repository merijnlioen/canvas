import Vector from "./vector";

export default class TrackBase {
    constructor() {
        this.trackParts = undefined
        this.trackPartsOriginal = undefined
        this.relativeTrack = undefined
        this.raceTrackImage = new Image()
        this.currentTrackPartIndex = -1
    }

    draw(canvas, ctx) {
        let size = this.calculateSize(canvas)

        ctx.moveTo(0,0)
        ctx.drawImage(this.raceTrackImage, 0, 0, size.width, size.height)

    }

    drawPath(ctx) {
        ctx.strokeStyle = 'red'
        
        ctx.beginPath()
        this.trackParts.map(part => {
            ctx.moveTo(part.start.x, part.start.y)
            ctx.quadraticCurveTo(part.curve.x, part.curve.y, part.end.x, part.end.y)
            ctx.stroke()
        })
    }

    calculateSize(canvas) {
        let ratioTrackImage = this.raceTrackImage.height / this.raceTrackImage.width
        let width = canvas.width
        let height = width * ratioTrackImage

        if (height > canvas.height) {
            height = canvas.height
            width = height / ratioTrackImage
        }

        return {
            width,
            height
        }
    }

    calculateRelativeCoordinates(canvas) {
        return new Promise(resolve => {
            this.raceTrackImage.addEventListener('load', () => {
                this.calculateRelativePosition(canvas)
                resolve()
            })
        })
    }

    calculateRelativePosition(canvas) {
        let trackSize = this.calculateSize(canvas)
        let ratio = 1
        
        if(trackSize.width > this.raceTrackImage.width) {
            ratio = trackSize.height / this.raceTrackImage.height
        } else {
            ratio = trackSize.width / this.raceTrackImage.width
        }

        this.trackParts.map((part) => {
            return {
                start: part.start.multiply(ratio),
                curve: part.curve.multiply(ratio),
                end: part.end.multiply(ratio)
            }
        })
    }

    calculateQuadraticBezierAtPosition(startPosition, controlPosition, endPosition, position) {

        position = position / 100

        let x = Math.pow(1 - position, 2) * startPosition.x + 2 * (1 - position) * position * controlPosition.x + Math.pow(position, 2) * endPosition.x
        let y = Math.pow(1 - position, 2) * startPosition.y + 2 * (1 - position) * position * controlPosition.y + Math.pow(position, 2) * endPosition.y
        
        return new Vector(x, y)
    }

    getNextTrackPart() {
        this.currentTrackPartIndex++
        
        if(this.currentTrackPartIndex === this.trackParts.length) {
            this.currentTrackPartIndex = 0
        }

        return this.trackParts[this.currentTrackPartIndex]
    }
}