import TrackBase from './track-base';
import Vector from './vector';

export default class TrackThree extends TrackBase {
    constructor(){
        super()
        this.raceTrackImage.src = './img/track_3.svg'

        this.trackParts = [
            {
                start: new Vector(153, 195),
                curve: new Vector(134, 231),
                end: new Vector(112, 270),
            },
            
            {
                start: new Vector(112, 270),
                curve: new Vector(95, 308),
                end: new Vector(115, 328),
            },
            
            {
                start: new Vector(115, 328),
                curve: new Vector(144, 354),
                end: new Vector(139, 365),
            },
            
            {
                start: new Vector(139, 365),
                curve: new Vector(133, 375),
                end: new Vector(189, 420),
            },
            
            {
                start: new Vector(189, 420),
                curve: new Vector(220, 437),
                end: new Vector(229, 384),
            },
            
            {
                start: new Vector(229, 384),
                curve: new Vector(229, 271),
                end: new Vector(258, 232),
            },
            
            {
                start: new Vector(258, 232),
                curve: new Vector(269, 204),
                end: new Vector(323, 240),
            },
            
            {
                start: new Vector(323, 240),
                curve: new Vector(363, 269),
                end: new Vector(385, 268),
            },
            
            {
                start: new Vector(385, 268),
                curve: new Vector(405, 257),
                end: new Vector(412, 291),
            },
            
            {
                start: new Vector(412, 291),
                curve: new Vector(410, 306),
                end: new Vector(430, 307),
            },
            
            {
                start: new Vector(430, 307),
                curve: new Vector(501, 311),
                end: new Vector(506, 288),
            },
            
            {
                start: new Vector(506, 288),
                curve: new Vector(510, 263),
                end: new Vector(544, 272),
            },
            
            {
                start: new Vector(544, 272),
                curve: new Vector(586, 271),
                end: new Vector(586, 293),
            },
            
            {
                start: new Vector(586, 293),
                curve: new Vector(592, 310),
                end: new Vector(670, 307),
            },
            
            {
                start: new Vector(670, 307),
                curve: new Vector(722, 311),
                end: new Vector(733, 290),
            },
            
            {
                start: new Vector(733, 290),
                curve: new Vector(753, 274),
                end: new Vector(740, 226),
            },
            
            {
                start: new Vector(740, 226),
                curve: new Vector(728, 182),
                end: new Vector(723, 129),
            },
            
            {
                start: new Vector(723, 129),
                curve: new Vector(708, 32),
                end: new Vector(690, 31),
            },
            
            {
                start: new Vector(690, 31),
                curve: new Vector(676, 29),
                end: new Vector(667, 20),
            },
            
            {
                start: new Vector(667, 20),
                curve: new Vector(654, 3),
                end: new Vector(639, 25),
            },
            
            {
                start: new Vector(639, 25),
                curve: new Vector(629, 63),
                end: new Vector(656, 107),
            },
            
            {
                start: new Vector(656, 107),
                curve: new Vector(689, 176),
                end: new Vector(622, 182),
            },
            
            {
                start: new Vector(622, 182),
                curve: new Vector(554, 188),
                end: new Vector(485, 185),
            },
            
            {
                start: new Vector(485, 185),
                curve: new Vector(458, 192),
                end: new Vector(409, 158),
            },
            
            {
                start: new Vector(409, 158),
                curve: new Vector(312, 80),
                end: new Vector(290, 145),
            },
            
            {
                start: new Vector(290, 145),
                curve: new Vector(267, 200),
                end: new Vector(222, 158),
            },
            
            {
                start: new Vector(222, 158),
                curve: new Vector(192, 119),
                end: new Vector(153, 195),
            },
            
        ]

        this.stops = [
            this.trackParts[0].start,
            this.trackParts[10].start,
            this.trackParts[17].start,
            this.trackParts[24].start,
        ]
    }
}

