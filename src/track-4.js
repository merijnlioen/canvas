import TrackBase from './track-base';
import Vector from './vector';

export default class TrackFour extends TrackBase {
    constructor(){
        super()
        this.raceTrackImage.src = './img/track_4.svg'

        this.trackParts = [
            {
                start: new Vector(630, 61),
                curve: new Vector(480, 83),
                end: new Vector(455, 68),
            },
            
            {
                start: new Vector(455, 68),
                curve: new Vector(419, 48),
                end: new Vector(317, 83),
            },
            
            {
                start: new Vector(317, 83),
                curve: new Vector(226, 105),
                end: new Vector(219, 145),
            },
            
            {
                start: new Vector(219, 145),
                curve: new Vector(224, 178),
                end: new Vector(197, 177),
            },
            
            {
                start: new Vector(197, 177),
                curve: new Vector(172, 177),
                end: new Vector(157, 198),
            },
            
            {
                start: new Vector(157, 198),
                curve: new Vector(126, 218),
                end: new Vector(114, 249),
            },
            
            {
                start: new Vector(114, 249),
                curve: new Vector(106, 274),
                end: new Vector(90, 274),
            },
            
            {
                start: new Vector(90, 274),
                curve: new Vector(76, 278),
                end: new Vector(49, 372),
            },
            
            {
                start: new Vector(49, 372),
                curve: new Vector(36, 412),
                end: new Vector(60, 427),
            },
            
            {
                start: new Vector(60, 427),
                curve: new Vector(92, 439),
                end: new Vector(107, 394),
            },
            
            {
                start: new Vector(107, 394),
                curve: new Vector(112, 362),
                end: new Vector(153, 363),
            },
            
            {
                start: new Vector(153, 363),
                curve: new Vector(190, 358),
                end: new Vector(210, 338),
            },
            
            {
                start: new Vector(210, 338),
                curve: new Vector(284, 291),
                end: new Vector(310, 305),
            },
            
            {
                start: new Vector(310, 305),
                curve: new Vector(330, 305),
                end: new Vector(492, 214),
            },
            
            {
                start: new Vector(492, 214),
                curve: new Vector(543, 187),
                end: new Vector(579, 158),
            },
            
            {
                start: new Vector(579, 158),
                curve: new Vector(611, 120),
                end: new Vector(643, 105),
            },
            
            {
                start: new Vector(643, 105),
                curve: new Vector(673, 82),
                end: new Vector(706, 64),
            },
            
            {
                start: new Vector(706, 64),
                curve: new Vector(735, 39),
                end: new Vector(724, 28),
            },
            
            {
                start: new Vector(724, 28),
                curve: new Vector(711, 9),
                end: new Vector(674, 36),
            },
            
            {
                start: new Vector(674, 36),
                curve: new Vector(651, 54),
                end: new Vector(630, 61),
            },
            
        ]

        this.stops = [
            this.trackParts[0].start,
            this.trackParts[5].start,
            this.trackParts[12].start,
            this.trackParts[15].start,
        ]
    }
}

